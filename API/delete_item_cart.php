<?php

include '../include/connect.php';

$response=array();
if ($_SERVER['REQUEST_METHOD']=='POST') {

    $id = $_POST['id'];

    $stmt=$db->prepare("DELETE FROM cart WHERE id=?");
    $stmt->execute(array($id));
    $count=$stmt->rowCount();       

    if($count > 0){

        $response['error']=false;
        $response['message']='Well done';

    }else{

        $response['error']=true;
        $response['message']='Invalid';

    }
}
echo json_encode($response);