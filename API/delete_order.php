<?php

/*---------- Require Parm -------------
-- order_id
---------------------------------------*/

if ($_SERVER['REQUEST_METHOD']=='POST' && isset($_REQUEST["order_id"])) {

    include '../include/connect.php';

    $response = array();

    $order_id = $_POST['order_id'];

    $stmt = $db->prepare("DELETE FROM orders WHERE id=?");
    $stmt->execute(array($order_id));
    $count = $stmt->rowCount();
    
    if($count > 0){
        $response['error'] = false;
        $response['message'] = 'Well done';
    } else {
        $response['error'] = true;
        $response['message'] = 'Invalid';
    }
    echo json_encode($response);
} else {
    echo "You Cannot Access This Page Directory.";
}
