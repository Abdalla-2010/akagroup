<?php

/*---------- Require Parm -------------
-- user_id
-- address_id
-- payment_type
-- coupon_id (Optional)
-- status_order -- (Hint) -- 0,1,2
-- order_key -- (Hint) -- Order Key this is Firebase Key
-- services_id -- (Hint) -- This is Array Of Objects Like -> [{"id":"1","quantity":"5"},{"id":"2","quantity":"5"},{"id":"3","quantity":"5"}]
-- products_id -- (Hint) -- This is Array Of Objects Like -> [{"id":"1","quantity":"5"},{"id":"2","quantity":"5"},{"id":"3","quantity":"5"}]
---------------------------------------*/

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if(isset($_REQUEST["user_id"]) && 
       isset($_REQUEST["address_id"]) && 
       isset($_REQUEST["payment_type"]) &&
       isset($_REQUEST["status_order"]) && 
       isset($_REQUEST["order_key"]) && ( isset($_REQUEST["services_id"]) || isset($_REQUEST["products_id"]) ) ) { // Check All Parmeter In Request

        include '../include/connect.php'; // Connect Into DB

        // Section User
        $user_id = $_POST['user_id']; 
        $address_id = $_POST['address_id']; 
        $payment_type = $_POST['payment_type'];

        // Section Details Order
        $status_order = $_POST['status_order'];
        $order_key = $_POST['order_key'];         

        // Insert Into Table Orders
        $stmt = $db->prepare("INSERT INTO orders(user_id,address_id,payment_type,status_order,order_key)  
                                  VALUES(:user_id,:address_id,:payment_type,:status_order,:order_key)");
        $stmt->execute(array('user_id' => $user_id,
                            'address_id' => $address_id,
                            'payment_type' => $payment_type,
                            'status_order' => $status_order,
                            'order_key' => $order_key ));
         $count = $stmt->rowCount();
         $order_id = $db->lastInsertId(); // Getting Order id

         if($count > 0) {
            if(isset($_REQUEST["services_id"])) { // Check If Request Have Services_Id
                $services_id = $_POST['services_id'];
                
                // Convert JSON string to Array Of Objects
                $arrayOfServices = json_decode($services_id);
            
                // Insert Services To Order Details
                if(is_array($arrayOfServices)){
                    for ($i = 0; $i < count($arrayOfServices) ; $i++) { 
                            $stmt = $db->prepare("INSERT INTO order_details(product_id,quantity,order_id)  
                                                    VALUES(:product_id,:quantity,:order_id)");
                            $stmt->execute(array('product_id' => $arrayOfServices[$i]->id, 'quantity' => $arrayOfServices[$i]->quantity,
                                                'order_id' => $order_id ));
                        }
                }

            } else if(isset($_REQUEST["products_id"])) { // Check If Request Have Products_Id
                $products_id = $_POST['products_id']; 

                // Convert JSON string to Array Of Objects
                $arrayOfProducts = json_decode($products_id);
            
                // Insert Products To Order Details
                if(is_array($arrayOfProducts)){
                    for ($i = 0; $i < count($arrayOfProducts) ; $i++) { 
                            $stmt = $db->prepare("INSERT INTO order_details(product_id,quantity,order_id)  
                                                    VALUES(:product_id,:quantity,:order_id)");
                            $stmt->execute(array('product_id' => $arrayOfProducts[$i]->id, 'quantity' => $arrayOfProducts[$i]->quantity,
                                                'order_id' => $order_id ));
                        }
                }

            }

            // Adding Coupon In Table
            if(isset($_REQUEST["coupon_id"])) {
                $coupon_id = $_POST['coupon_id'];
                $stmt = $db->prepare("INSERT INTO coupons_order(coupon_id,order_id)  
                                    VALUES(:coupon_id,:order_id)");
                $stmt->execute(array('coupon_id' => $coupon_id,
                            'order_id' => $order_id ));
            }

            // Deleting Items From Cart
            $stmt_cart = $db->prepare("DELETE FROM cart WHERE user_id=?");
            $stmt_cart->execute(array($user_id)); 

            $response['error'] = false;
            $response['message'] = 'Well done';
        
            echo json_encode($response);

         } else {

            $response['error'] = true;
            $response['message'] = 'Please Check Send All Ids';
        
            echo json_encode($response);
            
         }

    }

} else {
    echo "You Cannot Access This Page Directory.";
}

?>