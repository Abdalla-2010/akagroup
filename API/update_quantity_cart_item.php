<?php

/*---------- Require Parm -------------
-- id
-- quantity
---------------------------------------*/

$response = array();
if ($_SERVER['REQUEST_METHOD']=='POST') {

    if(isset($_REQUEST["id"]) && isset($_REQUEST["quantity"])) { // Check All Parmeter In Request

        include '../include/connect.php'; // Connect Into DB

        // Section Details Cart
        $id = $_POST['id']; 
        $quantity = $_POST['quantity'];
       
        // Update Status Into Table Cart
        $stmt = $db->prepare("UPDATE cart SET quantity = ? WHERE id = ? ");
        $stmt->execute(array($quantity, $id));
        $count = $stmt->rowCount();

         if($count > 0) {
            $response['error'] = false;
            $response['message'] = 'Well done';
         } else {
            $response['error'] = true;
            $response['message'] = 'Invalid';
         }

         echo json_encode($response);
    }

} else {
    echo "You Cannot Access This Page Directory.";
}

?>