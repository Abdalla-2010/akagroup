<?php

/*---------- Require Parm -------------
-- user_id
-- address_id
-- payment_type
-- coupon_id (Optional)
-- status_order -- (Hint) -- 0,1,2
-- order_key -- (Hint) -- Order Key this is Firebase Key
-- services_id -- (Hint) -- This is Array Of Objects Like -> [{"id":"1","quantity":"5"},{"id":"2","quantity":"5"},{"id":"3","quantity":"5"}]
-- products_id -- (Hint) -- This is Array Of Objects Like -> [{"id":"1","quantity":"5"},{"id":"2","quantity":"5"},{"id":"3","quantity":"5"}]
---------------------------------------*/

$response = array();

// Check Request Method Type From SERVER
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // Fetching Data From Json Object
    $content = trim(file_get_contents("php://input"));
    $decoded = json_decode($content, true);

    if(isset($decoded["user_id"]) && 
       isset($decoded["address_id"]) && 
       isset($decoded["payment_type"]) &&
       isset($decoded["status_order"]) && 
       isset($decoded["order_key"]) && 
       ( isset($decoded["services_id"]) || 
       isset($decoded["products_id"]) ) ) { // Check All Parmeter In Request

        include '../include/connect.php'; // Connect Into DB

        // Section User
        $user_id = $decoded['user_id']; 
        $address_id = $decoded['address_id']; 
        $payment_type = $decoded['payment_type'];

        // Section Details Order
        $status_order = $decoded['status_order'];
        $order_key = $decoded['order_key'];
        
        // Check Arrays
        checkArrays($decoded);

        // Insert Into Table Orders
        $stmt = $db->prepare("INSERT INTO orders(user_id,address_id,payment_type,status_order,order_key)  
                                  VALUES(:user_id,:address_id,:payment_type,:status_order,:order_key)");
        $stmt->execute(array('user_id' => $user_id,
                            'address_id' => $address_id,
                            'payment_type' => $payment_type,
                            'status_order' => $status_order,
                            'order_key' => $order_key ));
         $count = $stmt->rowCount();
         $order_id = $db->lastInsertId(); // Getting Order id

         if($count > 0) {
            if(isset($decoded["services_id"])) { // Check If Request Have Services_Id
                $services_id = $decoded['services_id'];
                
                // Insert Services To Order Details
                if(is_array($services_id)){
                     // Convert To Encode JSON Array Of Objects
                    $arrayOfServices = json_decode(json_encode($services_id));

                    for ($i = 0; $i < count($arrayOfServices) ; $i++) { 
                            $stmt = $db->prepare("INSERT INTO order_details(product_id,quantity,order_id)  
                                                    VALUES(:product_id,:quantity,:order_id)");
                            $stmt->execute(array('product_id' => $arrayOfServices[$i]->id, 'quantity' => $arrayOfServices[$i]->quantity,
                                                'order_id' => $order_id ));
                        }
                }

            } else if(isset($decoded["products_id"])) { // Check If Request Have Products_Id
                $products_id = $decoded['products_id']; 

                // Insert Products To Order Details
                if(is_array($products_id)){
                      // Convert To Encode JSON Array Of Objects
                    $arrayOfProducts = json_decode(json_encode($products_id));

                    for ($i = 0; $i < count($products_id) ; $i++) { 
                            $stmt = $db->prepare("INSERT INTO order_details(product_id,quantity,order_id)  
                                                    VALUES(:product_id,:quantity,:order_id)");
                            $stmt->execute(array('product_id' => $arrayOfProducts[$i]->id, 'quantity' => $arrayOfProducts[$i]->quantity,
                                                'order_id' => $order_id ));
                                                
                    }
                }

            }

            // Adding Coupon In Table
            if(isset($decoded["coupon_id"])) {
                $coupon_id = $decoded['coupon_id'];
                $stmt = $db->prepare("INSERT INTO coupons_order(coupon_id,order_id)  
                                    VALUES(:coupon_id,:order_id)");
                $stmt->execute(array('coupon_id' => $coupon_id,
                            'order_id' => $order_id ));
            }

            // Deleting Items From Cart
            $stmt_cart = $db->prepare("DELETE FROM cart WHERE user_id=?");
            $stmt_cart->execute(array($user_id)); 

            $response['error'] = false;
            $response['message'] = 'Well done';
            echo json_encode($response);

         } else {

            ErrorMessage('Please Check Send All Ids');
            
         }

    }

} else {
    echo "You Cannot Access This Page Directory.";
}

function checkArrays($decoded) {
    
    if(isset($decoded["products_id"])) {
        if(!is_array($decoded["products_id"])){
            ErrorMessage('Please Set in name (products_id) Should be Array Of Objects');  
        }
    }

    if(isset($decoded["services_id"])) {
        if(!is_array($decoded["services_id"])){
            ErrorMessage('Please Set in name (services_id) Should be Array Of Objects');  
        }
    }
}

function ErrorMessage($message) {
    $response['error'] = true;
    $response['message'] = $message;
    echo json_encode($response);
    exit();
}

?>