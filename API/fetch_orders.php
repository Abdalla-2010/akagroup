<?php

/*---------- Require Parm -------------
-- user_id
---------------------------------------*/
 
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_REQUEST["user_id"])) {

    include '../include/connect.php';

    $data = array();

    $user_id = $_POST['user_id']; 
    $stmt = $db->prepare("SELECT orders.id, orders.payment_type, orders.user_id, orders.address_id, 
    orders.status_order, orders.order_key, orders.date_time
                        FROM orders
                        WHERE orders.user_id = $user_id");
    $stmt->execute();
    $order_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $count = $stmt->rowCount();
    if($count > 0) {

        foreach($order_result as $row) {

            $result_orders = array();;
            $services_result = array();
            $products_result = array();
            $TotalPrice = 0;
            $quantity = "";
    
            // Adding Order
            $result_orders['order'] = $row;
    
            $order_id = $row['id'];
    
            $stmt_order_details = $db->prepare("SELECT * FROM order_details WHERE order_id = $order_id");
            $stmt_order_details->execute();
            $result_order_details = $stmt_order_details->fetchAll(PDO::FETCH_ASSOC);
    
            foreach($result_order_details as $row_order_details) {

                $product_id = $row_order_details['product_id'];
    
                $quantity = $row_order_details['quantity'];
                
                // Fetching Products 
                $stmt = $db->prepare("SELECT * FROM products WHERE products.id = $product_id");
                $stmt->execute();
                $num_rows_products = $stmt->rowCount();
                $products_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if($num_rows_products > 0) {
                    foreach($products_data as $row_product) {
    
                        $row_product['quantity'] = $quantity;
                        
                        if($row_product['status'] == 0) { // If Have Item Service
    
                            $services_result[] = $row_product;
                            // Adding Services
                            $result_orders['services'] = $services_result;
    
                        } else if($row_product['status'] == 1) { // If Have Item Product
    
                            $products_result[] = $row_product;
                            // Adding Products
                            $result_orders['products'] = $products_result;
                        }
                        
                        $TotalPrice += $quantity * $row_product["price"];
                    }
                }
            }
    
            $address_id = $row['address_id'];
             // Fetching Address User 
             $stmt_address_user = $db->prepare("SELECT * FROM address_user WHERE id = $address_id");
             $stmt_address_user->execute();
    
             // Adding Address
             $result_orders['address'] = $stmt_address_user->fetchAll(PDO::FETCH_ASSOC);
    
             // Fetching Coupon 
             $stmt_coupons_order = $db->prepare("SELECT coupon_id FROM coupons_order WHERE order_id = $order_id LIMIT 1");
             $stmt_coupons_order->execute();
             $result = $stmt_coupons_order->fetchAll(PDO::FETCH_ASSOC);
             $num_rows = $stmt_coupons_order->rowCount();
             if($num_rows > 0) {
                foreach($result as $row) {
                    $coupon_id = $row['coupon_id'];
                    $stmt_coupon = $db->prepare("SELECT * FROM coupon WHERE id = $coupon_id LIMIT 1");
                    $stmt_coupon->execute();
                    $result_coupon = $stmt_coupon->fetchAll(PDO::FETCH_ASSOC);
                    
                    foreach($result_coupon as $row) {
                        // Adding Coupon
                        $result_orders['coupon'] = $row;
                        // Totally Price After Discount
                        $TotalPrice = $TotalPrice - $row['value'];
                    }
                   
                }
             } else {
                 // Adding Coupon
                $result_orders['coupon'] = [];
             }
    
             $result_orders['TotalPrice'] = $TotalPrice;

             $data[] = $result_orders;

        }

        echo json_encode($data); // Show Response
 
    } else {
        $response['error'] = false;
        $response['message']='No Have Orders';
        echo json_encode($response);
    }

} else {
    echo "You Cannot Access This Page Directory.";
}

?>