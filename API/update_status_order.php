<?php

/*---------- Require Parm -------------
-- order_id
-- user_id
-- status_order
---------------------------------------*/

$response = array();
if ($_SERVER['REQUEST_METHOD']=='POST') {

    if(isset($_REQUEST["order_id"]) &&
       isset($_REQUEST["user_id"]) && 
       isset($_REQUEST["status_order"])) { // Check All Parmeter In Request

        include '../include/connect.php'; // Connect Into DB

        // Section Details Order
        $order_id = $_POST['order_id']; 
        $status_order = $_POST['status_order'];
        // Section User
        $user_id = $_POST['user_id']; 
       
        // Update Status Into Table Orders
        $stmt = $db->prepare("UPDATE orders SET status_order = ? WHERE id = ? AND user_id = ? ");
        $stmt->execute(array($status_order, $order_id, $user_id));
        $count = $stmt->rowCount();

         if($count > 0) {
            $response['error'] = false;
            $response['message'] = 'Well done';
         } else {
            $response['error'] = true;
            $response['message'] = 'Invalid';
         }

         echo json_encode($response);
    }

} else {
    echo "You Cannot Access This Page Directory.";
}

?>