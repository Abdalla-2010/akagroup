<?php
	
	include('../include/function.php');
	include('../firebase/function.php');
	if(isset($_POST["user_id"]) && isset($_POST["title"]) && isset($_POST["message"])) {

		include('../include/connect.php');

		$user_id = $_POST["user_id"];
		$title = $_POST["title"];
		$message = $_POST["message"];

		// Check Has Token In Token Users
		$query_token = "SELECT token_id FROM token_users WHERE user_id = $user_id LIMIT 1";
		$statement_token = $db->prepare($query_token);
		$statement_token->execute();
		$result = $statement_token->fetchAll(PDO::FETCH_ASSOC);
		$num_rows_token = $statement_token->rowCount();
		
		if($result > 0) { // Have Token

			foreach($result as $row) {
				// Send Message In Spicial Device
				if(sendSpicialMessage($row['token_id'], $title, $message)) {
					$response['error'] = false;
					$response['message'] = 'Well done';
					echo json_encode($response);
				} else {
					$response['error'] = true;
					$response['message'] = 'Something Went Wrong';
					echo json_encode($response);
				}
			}
		
		} else { // No Have Token
			$response['error'] = true;
			$response['message'] = 'Error Token';
			echo json_encode($response);
		}
	} else {
		echo "You Cannot Access This Page Directory.";
	}