<?php

$response=array();
if ($_SERVER['REQUEST_METHOD']=='POST' && isset($_REQUEST["about"])) {

  include '../include/connect.php';

    $about = $_POST['about'];
    $stmt = $db->prepare("UPDATE setting SET about = :about WHERE id = 1");
    $stmt->execute(array('about' => $about));
    $count = $stmt->rowCount();
    if($count > 0){
        $response['error']=false;
        $response['message']='Well done';
      }else{
          $response['error']=true;
          $response['message']='Invalid';
      }

}
echo json_encode($response);