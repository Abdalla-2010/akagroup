<?php

include '../include/connect.php';

$response=array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $user_id      = $_POST['user_id'];
    $product_id   = $_POST['product_id'];
    $quantity     = $_POST['quantity'];
    
$stmt=$db->prepare("INSERT INTO cart(user_id,quantity,product_id) VALUES(:user_id,:quantity,:product_id)");

      $stmt->execute(array('user_id'=>$user_id,
                           'quantity'=>$quantity,
                           'product_id'=>$product_id
                         ));
         $count=$stmt->rowCount();
         if($count > 0) {
            $response['error']=false;
            $response['message']='Well done';
         } else {
            $response['error']=true;
            $response['message']='Invalid';
        }

        echo json_encode($response);
}
