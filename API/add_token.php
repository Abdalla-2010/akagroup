  
<?php
	
	include('../include/function.php');
	if(isset($_POST["token_id"]) && isset($_POST["user_id"])) {

		include('../include/connect.php');

		$token_id = $_POST["token_id"];
		$user_id = $_POST["user_id"];

		// Check Has Token In Token Users
		$query_token = "SELECT id FROM token_users WHERE user_id = $user_id LIMIT 1";
		$statement_token = $db->prepare($query_token);
		$statement_token->execute();
		$num_rows_token = $statement_token->rowCount();

		if($num_rows_token > 0) { // Have Token
			// Updating Token To Token Users
			$statement = $db->prepare("UPDATE token_users SET token_id = :token_id 
			WHERE user_id = :user_id");
			$result = $statement->execute(array('token_id' => $token_id, 'user_id' => $user_id));

		} else { // No Have Token

			// Adding Token To Token Users
			$statement = $db->prepare("INSERT INTO token_users(token_id, user_id) 
			VALUES (:token_id, :user_id) ");
			$result = $statement->execute(
				array(
					'token_id'	=>	$token_id,
					'user_id' => $user_id
				)
			);
		}

		$response['error'] = false;
		$response['message'] = 'Well done';
		echo json_encode($response);
		
	} else {
		echo "You Cannot Access This Page Directory.";
	}