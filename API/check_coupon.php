<?php

/*---------- Require Parm -------------
-- coupon_name
---------------------------------------*/
 
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_REQUEST["coupon_name"])) {

    include '../include/connect.php';

    $coupon_name = $_POST['coupon_name']; 
    $stmt = $db->prepare("SELECT id, value, start_date, end_date FROM coupon WHERE name = '$coupon_name'");
    $stmt->execute();
    $coupon_result = $stmt->fetch(PDO::FETCH_ASSOC);
    $count = $stmt->rowCount();
    if($count > 0) {

        $start_date = $coupon_result['start_date'];
        $end_date = $coupon_result['end_date'];

        // Check Data Coupon
		
		date_default_timezone_set('Africa/Cairo');
        $current_Date = strtotime(date('Y/m/d', time()));
        
        $Start_Date_Str = strtotime($start_date);
        $End_Date_Str = strtotime($end_date);
        
		if($Start_Date_Str <= $current_Date && $End_Date_Str > $current_Date) {
            echo json_encode($coupon_result); // Show Response
		} else {
			$response['error'] = false;
            $response['message']='Ended Date';
            $response['start_date']= $start_date;
            $response['end_date']= $end_date;
            echo json_encode($response);
        } 
        
    } else {
        $response['error'] = false;
        $response['message']='Invalid';
        echo json_encode($response);
    }

} else {
    echo "You Cannot Access This Page Directory.";
}

?>