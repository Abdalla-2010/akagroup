<?php
    include '../include/connect.php';

    $user_id = $_POST['user_id'];

    $stmt=$db->prepare("SELECT cart.*, 
                                products.name, 
                                products.price, 
                                products.image, 
                                products.description, 
                                products.status, 
                                products.soft_delete, 
                                products.date_time
                            FROM cart 
                        INNER JOIN products ON cart.product_id = products.id 
                        WHERE user_id = ? ");
    $stmt->execute(array($user_id));
    $cat = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($cat);

?>