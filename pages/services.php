<?php
ob_start();
session_start();
if(isset($_SESSION['sess_user_id'])){ ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin AKA | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->

    <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Bootstrap 4 RTL -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
    <!-- Custom style for RTL -->
    <link rel="stylesheet" href="../dist/css/custom.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>  

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />

  </head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

 <!-- Navbar -->
  <?php include '../include/nav.php';?>
 <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    <?php include '../include/side-menu.php';?>
  <!-- /.Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">الخدمات</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        
  <div class="container box">
   <div class="table-responsive">
    <br />
    <div align="right">
     <button type="button" id="add_button" data-toggle="modal" data-target="#serviceModal" class="btn btn-info btn-lg">اضافة</button>
    </div>
    <br /><br />
    <table id="service_data" class="table table-striped table-bordered" style="width:100%">
     <thead>
      <tr>
       <th width="17%">اسم الخدمة</th>
       <th width="17%">تفاصيل الخدمة</th>
       <th width="17%">سعر الخدمة</th>
       <th width="12%">الصورة</th>
       <th width="17%">الوقت والتاريخ</th>
       <th width="10%">تعديل</th>
       <th width="10%">حذف</th>
      </tr>
     </thead>
    </table>
    
   </div>
  </div>

<div id="serviceModal" class="modal fade">
 <div class="modal-dialog">
  <form method="post" id="service_form" enctype="multipart/form-data">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
     <h4 class="modal-title">اضافة خدمة </h4>
    </div>
    <div class="modal-body">
     <label>اسم الخدمة</label>
     <input type="text" name="name" id="name" class="form-control" require />
     <br />
     <label>تفاصيل الخدمة</label>
     <textarea type="text" name="description" id="description" class="form-control" require></textarea>
     <br />
     <label>سعر الخدمة</label>
     <input type="number" name="price" id="price" class="form-control" require />
     <br />
     <label>الصورة</label>
     <input type="file" name="service_image" id="service_image" />
     <span id="service_uploaded_image"></span>
    </div>
    <div class="modal-footer">
     <input type="hidden" name="service_id" id="service_id" />
     <input type="hidden" name="operation" id="operation" />
     <input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
   </div>
  </form>
 </div>
</div>


<script type="text/javascript" language="javascript">

  $(document).ready(function() {
      $('#example').DataTable();
  } );

  $(document).ready(function(){

  // When Click Add Btn  
  $('#add_button').click(function(){
    $('#service_form')[0].reset();
    $('.modal-title').text("Add Service");
    $('#action').val("Add");
    $('#operation').val("Add");
    $('#service_uploaded_image').html('');
  });
  
  // Fetch Data
  var dataTable = $('#service_data').DataTable({
    "processing":true,
    "serverSide":true,
    "order":[],
    "ajax":{
      url:"../ajax/fetch_services.php",
      type:"POST"
    },
    "columnDefs":[
      {
        "targets":[0, 3, 4],
        "orderable":false,
      },
    ],

  });

  // Insert Data
  $(document).on('submit', '#service_form', function(event){
    event.preventDefault();
    var name = $('#name').val();
    var description = $('#description').val();
    var price = $('#price').val();
    var operation = $('#operation').val();
    var extension = $('#service_image').val().split('.').pop().toLowerCase();

    if(operation == "Add") { // Add Form

      if(extension != '') {
          if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1) {
            alert("Invalid Image File");
            $('#service_image').val('');
            return false;
          }
      }
      
      if(name != '' && description != '' && price != '' && extension != '') {
        $.ajax({
          url:"../ajax/insert_service.php",
          method:'POST',
          data:new FormData(this),
          contentType:false,
          processData:false,
          success:function(data) {
            $('#service_form')[0].reset();
            $('#serviceModal').modal('hide');
            dataTable.ajax.reload();
          }
        });
      } else {
        if(extension == '' && name != '' && description != '' && price != '') {
          alert("Image File Require");
        } else {
          alert("Fields are Required");
        }
      }

    } else if(operation == "Edit") { // Edit Form

      var imageThumbnailExtension = $('#hidden_service_image').val().split('.').pop().toLowerCase();
      if(imageThumbnailExtension != '') {
          if(jQuery.inArray(imageThumbnailExtension, ['gif','png','jpg','jpeg']) == -1) {
            alert("Invalid Image File");
            $('#hidden_service_image').val('');
            return false;
          }
      }
      
      if(name != '' && description != '' && price != '' && imageThumbnailExtension != '') {
          $.ajax({
            url:"../ajax/insert_service.php",
            method:'POST',
            data:new FormData(this),
            contentType:false,
            processData:false,
            success:function(data) {
              $('#service_form')[0].reset();
              $('#serviceModal').modal('hide');
              dataTable.ajax.reload();
            }
          });
        } else {
          if(imageThumbnailExtension == '' && name != '' && description != '' && price != '') {
            alert("Image File Require");
          } else {
            alert("Fields are Required");
          }
        }
    }
    
  });
  
  // Click Edit Btn  
  $(document).on('click', '.update', function() {
    var service_id = $(this).attr("id");
    $.ajax({
    url:"../ajax/fetch_single_service.php",
    method:"POST",
    data:{service_id:service_id},
    dataType:"json",
    success:function(data) {
        $('#serviceModal').modal('show');
        $('.modal-title').text("Edit Service");
        $('#name').val(data.name);
        $('#description').val(data.description);
        $('#price').val(data.price);
        $('#service_id').val(service_id);
        $('#service_uploaded_image').html(data.service_image);
        $('#action').val("Edit");
        $('#operation').val("Edit");
      }
    })
  });
  
  // Delete Service
  $(document).on('click', '.delete', function() {
    var service_id = $(this).attr("id");
    if(confirm("Are you sure you want to delete this?")) {
      $.ajax({
        url:"../ajax/delete_service.php",
        method:"POST",
        data:{service_id:service_id},
        success:function(data)
        {
        alert(data);
        dataTable.ajax.reload();
        }
      });
    } else {
      return false; 
    }
  });
  
  
  });
</script>
  
        <!-- /.row -->
        <!-- Main row -->
   
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main footer -->
   <?php include '../include/footer.php';?>
  <!-- /.footer -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 rtl -->
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../plugins/jqvmap/maps/jquery.vmap.world.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>


<?php       
    }else{
       echo 'You are not allawed to come here';
    }