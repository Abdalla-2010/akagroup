<?php
ob_start();
session_start();
if(isset($_SESSION['sess_user_id'])){ ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin AKA | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->

    <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Bootstrap 4 RTL -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
    <!-- Custom style for RTL -->
    <link rel="stylesheet" href="../dist/css/custom.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>  

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />

  </head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

 <!-- Navbar -->
  <?php include '../include/nav.php';?>
 <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include '../include/side-menu.php';?>
  <!-- /.Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">الطلبات</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

  <div class="container box">
   <div class="table-responsive">
    <br /><br />
    <table id="orders_data" class="table table-striped table-bordered" style="width:100%">
     <thead>
      <tr>
       <th width="10%">ID</th>
       <th width="10%">اسم العميل</th>
       <th width="15%">اسم الخدمة</th>
       <th width="15%">سعر الخدمة</th>
       <th width="5%">الكمية</th>
       <th width="5%">خصم</th>
       <th width="10%">اجمالي الطلب</th>
       <th width="10%">اسم الطيار</th>
       <th width="10%">حالة الطلب</th>
       <th width="10%">الوقت والتاريخ</th>
       <th width="10%">أجراءات</th>
      </tr>
     </thead>
    </table>
   </div>
  </div>

<div id="orderModal" class="modal fade">
 <div class="modal-dialog">
  <form method="post" id="order_form" enctype="multipart/form-data">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
     <h4 class="modal-title">عرض الطلب </h4>
    </div>
    <div class="modal-body">
     <label>رقم الطلب</label>
     <p name="order_id" id="order_id" class="view-data"></p>
     <br />
     <label>اسم العميل</label>
     <p name="name_customer" id="name_customer" class="view-data"></p>
     <br />
     <label>رقم العميل</label>
     <p name="phone_customer" id="phone_customer" class="view-data"></p>
     <br />
     <label>عنوان العميل</label>
     <p name="address_customer" id="address_customer" class="view-data"></p>
     <br />
     <label>الكمية</label>
     <p name="quantity" id="quantity" class="view-data"></p>
     <br />
     <label>نوع الخدمة</label>
     <p name="product_type" id="product_type" class="view-data"></p>
     <br />
     <label>اسم الخدمة</label>
     <p name="products_name" id="products_name" class="view-data"></p>
     <br />
     <label>سعر الخدمة</label>
     <p name="products_price" id="products_price" class="view-data"></p>
     <br />
     <label>كوبون</label>
     <p name="coupon" id="coupon" class="view-data"></p>
     <br />
     <label>خصم</label>
     <p name="coupon_price" id="coupon_price" class="view-data"></p>
     <br />
     <label>اجمالي سعر الطلب</label>
     <p name="totally_price" id="totally_price" class="view-data"></p>
     <br />
     <label>نوع الدفع</label>
     <p name="payment_type" id="payment_type" class="view-data"></p>
     <br />
     <label>اسم الطيار</label>
     <p name="delivery_name" id="delivery_name" class="view-data"></p>
     <br />
     <label>رقم الطيار</label>
     <p name="delivery_phone" id="delivery_phone" class="view-data"></p>
     <br />
     <label>حالة الطلب</label>
     <p name="order_status" id="order_status" class="view-data"></p>
     <br />
     <label>وقت وتاريخ الطلب</label>
     <p name="order_date_time" id="order_date_time" class="view-data"></p>
     <label>تغير حالة الطلب</label>
     <select class="custom-select" id="status_order_select" name="status_order_select">
        <?php
            include('../include/connect.php'); 
            // Start Getting Status Of Order
            $query_status = "SELECT * FROM status_order";
            $statement_status = $db->prepare($query_status);
            $statement_status->execute();
            $result_status = $statement_status->fetchAll(PDO::FETCH_ASSOC);
            $num_rows_status = $statement_status->rowCount();
        
            if($num_rows_status > 0) {
              foreach($result_status as $row_status) {
                $status_order_row = $row_status['status_order'];
                $name_ar_row = $row_status['name_ar'];

                $status_order = $name_ar_row;
                echo " <option value='".$status_order_row."'>".$status_order."</option>";
              }
            }
            // End Getting Status Of Order
        ?>
      </select>
      <br />
      <br />
     <label>تعيين طيار</label>
     <select class="custom-select" id="delivery_select" name="delivery_select">
    </select>
   
    </div>
    <div class="modal-footer">
     <input type="hidden" name="token_fcm" id="token_fcm" />
     <input type="hidden" name="key_firebase" id="key_firebase" />
     <input type="hidden" name="order_id_edit" id="order_id_edit" />
     <input type="submit" name="action" id="action" class="btn btn-success" value="تحديث" />
     <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
    </div>
   </div>
  </form>
 </div>
</div>


<script type="text/javascript" language="javascript">

  $(document).ready(function() {
      $('#example').DataTable();
  } );

  // Reset Data Model When Closed
  $(document).on('hide.bs.modal', '#orderModal', function (e) {
        $('#delivery_select').empty();
        $('#status_order_select option:selected').removeAttr('selected');
    });


  $(document).ready(function(){
  
  // Fetch Data
  var dataTable = $('#orders_data').DataTable({
    "processing":true,
    "serverSide":true,
    "order":[],
    "ajax":{
      url:"../ajax/fetch_orders.php",
      type:"POST"
    },
    "columnDefs":[
      {
        "targets":[0, 3, 4],
        "orderable":false,
      },
    ],

  });

  // Update Data
  $(document).on('submit', '#order_form', function(event){
    event.preventDefault();

    var status_order = $('#status_order_select').val();
    var delivery_name = $('#delivery_select').val();

    if(status_order != '0' && delivery_name != '0') {
        $.ajax({
          url:"../ajax/update_order.php",
          method:'POST',
          data:new FormData(this),
          contentType:false,
          processData:false,
          success:function(data) {
            $('#order_form')[0].reset();
            $('#orderModal').modal('hide');
            dataTable.ajax.reload();
          }
        });
      } else {
        alert("Fields are Required");
      }
  });
  
  // Click View Btn  
  $(document).on('click', '.view', function() {
    var order_id = $(this).attr("id");
    $.ajax({
    url:"../ajax/fetch_single_order.php",
    method:"POST",
    data:{order_id:order_id},
    dataType:"json",
    success:function(data) {
        $('#orderModal').modal('show');
        $('.modal-title').text("عرض الطلب");
        $('#order_id_edit').val(order_id);
        $('#order_id').text(data.order_id);
        $('#name_customer').text(data.name_customer);
        $('#phone_customer').text(data.phone_customer);
        $('#address_customer').text(data.address_customer);
        $('#quantity').text(data.quantity);
        $('#product_type').text(data.product_type);
        $('#products_name').text(data.products_name);
        $('#products_price').text(data.products_price);
        $('#coupon').text(data.coupon);
        $('#coupon_price').text(data.coupon_price);
        $('#totally_price').text(data.totally_price);
        $('#payment_type').text(data.payment_type);
        $('#delivery_name').text(data.delivery_name);
        $('#delivery_phone').text(data.delivery_phone);

        $('#order_status').text(data.order_status);
        $("#status_order_select option[value='" + data.order_status_value +"']").attr("selected","selected");

        $('#order_date_time').text(data.order_date_time);

        $('#delivery_select').append(data.all_delivery);
        $("#delivery_select option[value='" + data.delivery_id +"']").attr("selected","selected");

        $('#token_fcm').val(data.token_fcm);
        $('#key_firebase').val(data.key_firebase);

        $('#order_id').val(order_id);
        $('#action').val("تحديث");
      }
    })
  });
  
  // Delete Order
  $(document).on('click', '.delete', function() {
    var order_id = $(this).attr("id");
    if(confirm("Are you sure you want to delete this?")) {
      $.ajax({
        url:"../ajax/delete_order.php",
        method:"POST",
        data:{order_id:order_id},
        success:function(data)
        {
        alert(data);
        dataTable.ajax.reload();
        }
      });
    } else {
      return false; 
    }
  });
  
  });
</script>
        <!-- /.row -->
        <!-- Main row -->
   
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   <!-- Main footer -->
   <?php include '../include/footer.php';?>
  <!-- /.footer -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 rtl -->
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../plugins/jqvmap/maps/jquery.vmap.world.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>


<?php       
    }else{
       echo 'You are not allawed to come here';
    }