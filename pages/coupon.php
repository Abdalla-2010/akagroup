<?php
ob_start();
session_start();
if(isset($_SESSION['sess_user_id'])){

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin AKA | Log in</title>
   <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->

  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Bootstrap 4 RTL -->
  <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
  <!-- Custom style for RTL -->
  <link rel="stylesheet" href="../dist/css/custom.css">

  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>  

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />


</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include '../include/nav.php';?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
   <?php include '../include/side-menu.php';?>
  <!-- /.Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">الكوبونات</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->





  <div class="container box">



   <div class="table-responsive">
    <br />
    <div align="right">
     <button type="button" id="add_button" data-toggle="modal" data-target="#couponModal" class="btn btn-info btn-lg">اضافة</button>
    </div>
    <br /><br />
    <table id="coupon_data" class="table table-striped table-bordered" style="width:100%">
     <thead>
      <tr>
       <th width="12%"> اسم الكوبون</th>
       <th width="12%"> قيمة الخصم </th>
       <th width="16%"> تعريف  الكوبون </th>
       <th width="12%"> سارى من    </th>
       <th width="12%">   الى  </th>

       <th width="12%">الصورة</th>
       <th width="12%">تعديل</th>
       <th width="12%">حذف</th>
      </tr>
     </thead>
    </table>
    
   </div>
  </div>


<div id="couponModal" class="modal fade">
 <div class="modal-dialog">
  <form method="post" id="coupon_form" enctype="multipart/form-data">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">اضافة كوبون</h4>
    </div>
    <div class="modal-body">
     <label>اسم الكوبون</label>
     <input type="text" name="name" id="name" class="form-control" require />
     <br />
     <label>قيمة الخصم </label>
     <input type="number" step="0.01" name="value" id="value" class="form-control" require />
     <br />
     <label>تعريف  الكوبون </label>
     <textarea type="text" name="definition" id="definition" class="form-control" require></textarea>
     <br />
     <label>سارى من </label>
     <input type="date" name="start_date" id="start_date" class="form-control" require />
     <br />
     <label>سارى الي </label>
     <input type="date" name="end_date" id="end_date" class="form-control" require />
     <br />
     <label>الصورة</label>
     <input type="file" name="coupon_image" id="coupon_image" />
     <span id="coupon_uploaded_image"></span>
    </div>
    <div class="modal-footer">
     <input type="hidden" name="coupon_id" id="coupon_id" />
     <input type="hidden" name="operation" id="operation" />
     <input type="submit" name="action" id="action" class="btn btn-success" value="أضافة" />
     <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
    </div>
   </div>
  </form>
 </div>
</div>












<script type="text/javascript" language="javascript" >
$(document).ready(function() {
    $('#example').DataTable();
} );

$(document).ready(function(){
 $('#add_button').click(function(){
  $('#coupon_form')[0].reset();
  $('.modal-title').text("أضافة كوبون");
  $('#action').val("أضافة");
  $('#operation').val("Add");
  $('#coupon_uploaded_image').html('');
 });
 
 var dataTable = $('#coupon_data').DataTable({
  "processing":true,
  "serverSide":true,
  "order":[],
  "ajax":{
   url:"../ajax/fetch_coupon.php",
   type:"POST"
  },
  "columnDefs":[
   {
    "targets":[0, 3, 4],
    "orderable":false,
   },
  ],

 });

 $(document).on('submit', '#coupon_form', function(event){
    event.preventDefault();
    var name = $('#name').val();
    var value = $('#value').val();
    var definition  = $('#definition').val();
    var start_date  = $('#start_date').val();
    var end_date  = $('#end_date').val();
    var operation = $('#operation').val();
    var extension = $('#coupon_image').val().split('.').pop().toLowerCase();

    if(operation == "Add") { // Add Form

      if(extension != '') {
          if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1) {
            alert("Invalid Image File");
            $('#coupon_image').val('');
            return false;
          }
      }
      
      if(name != '' && value != '' && definition != '' && start_date != '' && end_date != '' && extension != '') {
        $.ajax({
          url:"../ajax/insert_coupon.php",
          method:'POST',
          data:new FormData(this),
          contentType:false,
          processData:false,
          success:function(data) {
            $('#coupon_form')[0].reset();
            $('#couponModal').modal('hide');
            dataTable.ajax.reload();
          }
        });
      } else {
        if(extension == '' && name != '' && value != '' && definition != '' && start_date != '' && end_date != '') {
          alert("Image File Require");
        } else {
          alert("Fields are Required");
        }
      }

    } else if(operation == "Edit") { // Edit Form

      var imageThumbnailExtension = $('#hidden_coupon_image').val().split('.').pop().toLowerCase();
      if(imageThumbnailExtension != '') {
          if(jQuery.inArray(imageThumbnailExtension, ['gif','png','jpg','jpeg']) == -1) {
            alert("Invalid Image File");
            $('#hidden_coupon_image').val('');
            return false;
          }
      }
      
      if(name != '' && value != '' && definition != '' && start_date != '' && end_date != '' && imageThumbnailExtension != '') {
          $.ajax({
            url:"../ajax/insert_coupon.php",
            method:'POST',
            data:new FormData(this),
            contentType:false,
            processData:false,
            success:function(data) {
              $('#coupon_form')[0].reset();
              $('#couponModal').modal('hide');
              dataTable.ajax.reload();
            }
          });
        } else {
          if(imageThumbnailExtension == '' && name != '' && value != '' && definition != '' && start_date != '' && end_date != '') {
            alert("Image File Require");
          } else {
            alert("Fields are Required");
          }
        }
    }
  
 });
 
 $(document).on('click', '.update', function(){
  var coupon_id = $(this).attr("id");
  $.ajax({
   url:"../ajax/fetch_single_coupon.php",
   method:"POST",
   data:{coupon_id:coupon_id},
   dataType:"json",
   success:function(data)
   {
    $('#couponModal').modal('show');
    $('.modal-title').text("تعديل كوبون");

    $('#name').val(data.name);
    $('#value').val(data.value);
    $('#definition').val(data.definition);
    $('#start_date').val(data.start_date);
    $('#end_date').val(data.end_date);

    $('#coupon_id').val(coupon_id);
    $('#coupon_uploaded_image').html(data.coupon_image);
    $('#action').val("تعديل");
    $('#operation').val("Edit");
   }
  })
 });
 
 $(document).on('click', '.delete', function(){
  var coupon_id = $(this).attr("id");
  if(confirm("Are you sure you want to delete this?"))
  {
   $.ajax({
    url:"../ajax/delete_coupon.php",
    method:"POST",
    data:{coupon_id:coupon_id},
    success:function(data)
    {
     alert(data);
     dataTable.ajax.reload();
    }
   });
  }
  else
  {
   return false; 
  }
 });
 
 
});
</script>
   


        <!-- /.row -->
        <!-- Main row -->
   
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main footer -->
    <?php include '../include/footer.php';?>
  <!-- /.footer -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 rtl -->
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../plugins/jqvmap/maps/jquery.vmap.world.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>


<?php       
    }else{
       echo 'You are not allawed to come here';
    }