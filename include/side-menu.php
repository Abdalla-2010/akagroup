<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link">
      <img src="../dist/img/AdminLTELogo.png"  class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AKA Group </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="../pages/customers.php" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
              إدارة المستخدمين
              </p>
            </a>
          </li>

         <li class="nav-item">
            <a href="../pages/manage_delivers.php" class="nav-link">
              <i class="nav-icon fas fa-motorcycle"></i>
              <p>
              إدارة المناديب
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../pages/coupon.php" class="nav-link">
              <i class="nav-icon fas fa-gift"></i>
              <p>
              الكوبونات
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../pages/product.php" class="nav-link">
              <i class="nav-icon fas fa-shopping-bag"></i>
              <p>
              المنتجات
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../pages/services.php" class="nav-link">
              <i class="nav-icon fas fa-server"></i>
              <p>
                الخدمات
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="ads.php" class="nav-link">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                الاعلانات
              </p>
            </a>
          </li> 


          <li class="nav-item">
            <a href="../pages/ui.php" class="nav-link">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                وجهات التطبيق 
              </p>
            </a>
          </li> 


          <li class="nav-item">
            <a  class="nav-link">
              <i class="nav-icon fas fa-credit-card"></i>
              <p>
                المدفوعات
              </p>
            </a>
          </li> 

          <li class="nav-item">
            <a href="../pages/orders.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                الطلبات
              </p>
            </a>
          </li> 

          <li class="nav-item">
            <a href="notifications.php" class="nav-link">
              <i class="nav-icon fas fa-child"></i>
              <p>
              الاشعارات
              </p>
            </a>
          </li> 

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>