<?php

	// Passing a New Param
	function upload_image($image)
	{
		if(isset($_FILES[$image]))
		{ 
			$extension = explode('.', $_FILES[$image]['name']);
			$new_name = rand() . '.' . $extension[1];
			$destination = '../upload/' . $new_name;
			move_uploaded_file($_FILES[$image]['tmp_name'], $destination);
			return $new_name;
		}
	}

	// Change And Adding New Param
	function get_image_name($table_name, $id) {
		include('connect.php');
		$statement = $db->prepare("SELECT image FROM $table_name WHERE id = '$id'");
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		foreach($result as $row) {
			return $row["image"];
		}
	}

	// Passing a New Param
	function get_total_all_records($table_name) {
		include('connect.php');
		$statement = $db->prepare("SELECT * FROM $table_name");
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $statement->rowCount();
	}
	