<?php
include('../include/connect.php');
include('../include/function.php');
if(isset($_POST["order_id"])) {

	$output = array();
	$statement = $db->prepare(
		"SELECT orders.*,
					 users.name AS 'user_name',
					 users.phone AS 'user_phone',
					 address_user.addres AS 'user_address',
					 address_user.street_name AS 'user_street_name',
					 address_user.street_number AS 'user_street_number',
					 address_user.home_number AS 'user_home_number',
					 address_user.lat AS 'user_lat',
					 address_user.longit AS 'user_long', 
					 token_users.token_id AS 'token_fcm' FROM orders
				INNER JOIN users 
					ON orders.user_id = users.id
				INNER JOIN address_user 
					ON orders.address_id = address_user.id
				INNER JOIN token_users 
					ON users.id = token_users.user_id
				WHERE orders.id = '".$_POST["order_id"]."' LIMIT 1"
	);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $row) {

		$result_final_product = '';
		$result_final_price_product = '';
		$TotalPrice = 0;
		$quantity = 0;
		$result_final_quantity = '';
		$product_type = '';
		$countProducts = 1;

		$orderID = $row['id'];

		// Start Section Details Order
		$query_order_details = "SELECT product_id,quantity  FROM order_details WHERE order_id = $orderID ";
		$statement_order_details = $db->prepare($query_order_details);
		$statement_order_details->execute();
		$result_order_details = $statement_order_details->fetchAll(PDO::FETCH_ASSOC);
		foreach($result_order_details as $row_order_details) {
			$productID = $row_order_details['product_id'];
			$quantity = $row_order_details['quantity'];

			// Getting All Quantity
			$result_final_quantity .= " (".$countProducts.") ". $quantity ;

			// Start Section Products
			$query_products = "SELECT products.name AS 'product_name',
									products.price AS 'product_price',
									products.status AS 'product_status',
									products.image AS 'product_image'
								FROM products 
									WHERE id = $productID ";
			$statement_products = $db->prepare($query_products);
			$statement_products->execute();
			$result_products = $statement_products->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($result_products as $row_product) {
				$result_final_product .= " (".$countProducts.") ".$row_product["product_name"] ;
				$result_final_price_product .= "(".$countProducts.") " .$row_product["product_price"]. " جنية " ;
				$TotalPrice += $quantity * $row_product["product_price"];

				if($row_product["product_status"] == 0) {
					$product_type .= "(".$countProducts.") ". " خدمة "; // Set Product Type
				} else {
					$product_type .= "(".$countProducts.") ". " منتج "; // Set Product Type
				}

				$countProducts++;
			}
			// End Section Products
		}
		// End Section Details Order
		
		// Start Section Delivery
			$query_delivery = "SELECT delivery.*, users.id AS 'delivery_id', users.name AS 'delivery_name', users.phone As 'delivery_phone' FROM delivery 
								INNER JOIN users 
									ON delivery.delivery_id = users.id
								WHERE delivery.order_id = $orderID 
									AND users.group_id = '1' LIMIT 1";
			$statement_delivery = $db->prepare($query_delivery);
			$statement_delivery->execute();
			$result_delivery = $statement_delivery->fetchAll(PDO::FETCH_ASSOC);
			$num_rows_delivery = $statement_delivery->rowCount();
			$deliveryId = '';
			$deliveryName = '';
			$deliveryPhone = '';
			if($num_rows_delivery == 1) {
				foreach($result_delivery as $row_delivery) {
					$deliveryId = $row_delivery["delivery_id"];
					$deliveryName = $row_delivery["delivery_name"];
					$deliveryPhone = $row_delivery["delivery_phone"];
				}
			} else {
				$deliveryId = '0';
				$deliveryName = "لم يتم تعيين طيار.";
				$deliveryPhone = "لا يوجد.";
			}

			// Getting All Deliverys
			$query_all_delivery = "SELECT id AS 'delivery_id', name AS 'delivery_name' FROM users 
								WHERE  users.group_id = '1'";
			$statement_all_delivery = $db->prepare($query_all_delivery);
			$statement_all_delivery->execute();
			$result_all_delivery = $statement_all_delivery->fetchAll(PDO::FETCH_ASSOC);
			$num_rows_all_delivery = $statement_all_delivery->rowCount();
			$all_delivery = "<option value='0' selected>اختار طيار</option>";
			if($num_rows_all_delivery > 0) {
				foreach($result_all_delivery as $row_delivery) {
					$all_delivery .= " <option value='".$row_delivery['delivery_id']."'>".$row_delivery['delivery_name']."</option>";
				}
			}
		// End Section Delivery

		// Start Section Coupon
		$query_coupon = "SELECT coupons_order.*, coupon.name AS 'coupon_name', coupon.value As 'coupon_value' FROM coupons_order 
							INNER JOIN coupon 
								ON coupons_order.coupon_id = coupon.id
							WHERE coupons_order.order_id = $orderID LIMIT 1";
		$statement_coupon = $db->prepare($query_coupon);
		$statement_coupon->execute();
		$result_coupon = $statement_coupon->fetchAll(PDO::FETCH_ASSOC);
		$num_rows_coupon = $statement_coupon->rowCount();
		$CouponPrice = '';
		$hasCoupon = '';
		if($num_rows_coupon == 1) {
			foreach($result_coupon as $row_coupon) {
				$CouponPrice = $row_coupon["coupon_value"];
				$hasCoupon = "نعم";
			}
		} else {
			$CouponPrice = "0";
			$hasCoupon = "لا";
		}
		// End Section Coupon

		// Totally Price After Discount
		$TotalPrice = $TotalPrice - $CouponPrice;

		
		// Start Getting Status Of Order
		$status = $row["status_order"];
		$query_status = "SELECT * FROM status_order WHERE status_order = $status LIMIT 1";
		$statement_status = $db->prepare($query_status);
		$statement_status->execute();
		$result_status = $statement_status->fetchAll(PDO::FETCH_ASSOC);
		$num_rows_status = $statement_status->rowCount();

		$status_order = '';
		if($num_rows_status > 0) {
			foreach($result_status as $row_status) {
				$status_order_row = $row_status['status_order'];
				$name_ar_row = $row_status['name_ar'];
				if($status == $status_order_row) {
					$status_order = $name_ar_row;
				}
			}
		}
		// End Getting Status Of Order
		

		// Data
		$output["order_id"] = $row["id"]; // id Of Order
		$output["name_customer"] = $row["user_name"]; // Name Of Customer
		$output["phone_customer"] = $row["user_phone"]; // Phone Of Customer
		$output["address_customer"] = $row["user_address"] ." - ". $row["user_street_name"] ." - ". $row["user_street_number"] ." - ". $row["user_home_number"]; // Address Of Customer
		$output["quantity"] = $result_final_quantity; // Quantity Of Order
		$output["product_type"] = $product_type; // Product Type Of Order
		$output["products_name"] = $result_final_product; // Name Of Products 
		$output["products_price"] = $result_final_price_product; // Price Of Products
		$output["coupon"] = $hasCoupon; // Has Coupon
		$output["coupon_price"] = $CouponPrice .' جنية'; // Price Of Coupon
		$output["totally_price"] = $TotalPrice .' جنية'; // Totally Price Of Order
		$output["payment_type"] = $row["payment_type"]; // Payment Type Of Order

		$output["delivery_id"] = $deliveryId; // Id Of Delevery 
		$output["delivery_name"] = $deliveryName; // Name Of Delevery 
		$output["delivery_phone"] = $deliveryPhone; // Phone Of Delevery 
		$output["order_status_value"] = $status; // Status Of Order 
		$output["order_status"] = $status_order; // Status Of Order 
		$output["order_date_time"] = $row["date_time"]; // DateTime Of Order

		$output["all_delivery"] = $all_delivery; // All Delivery

		$output["token_fcm"] = $row["token_fcm"]; // Token Id FCM
		$output["key_firebase"] = $row["order_key"]; // Key Firebase Realtime Of Order
	}

	echo json_encode($output);
}