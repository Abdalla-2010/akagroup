<?php

session_start(); 
include("../include/connect.php");

if(isset($_POST['username']) && $_POST['username'] != '' && isset($_POST['password']) && $_POST['password'] != '') {

	$username = trim($_POST['username']);
	$password = trim($_POST['password']);

	if($username != "" && $password != "") {
		try {

			$query = "select * from `users` where `phone`=:username and `password`=:password";
			$stmt = $db->prepare($query);
			$stmt->bindParam('username', $username, PDO::PARAM_STR);
			$stmt->bindValue('password', $password, PDO::PARAM_STR);
			$stmt->execute();
			$count = $stmt->rowCount();
			$row   = $stmt->fetch(PDO::FETCH_ASSOC);

			if($count >0) {
				/******************** Your code ***********************/
				$_SESSION['sess_user_id']   = $row['id'];
				$_SESSION['sess_username'] = $row['name'];
				$_SESSION['sess_phone'] = $row['phone'];
				echo "pages/dashboard.php";
			} else {
				echo "invalid";
			}
		} catch (PDOException $e) {
			echo "Error : ".$e->getMessage();
		}
	} else {
		echo "Both fields are required!";
	}

} 

else {
	header('location:./dashboard');
}
?>
