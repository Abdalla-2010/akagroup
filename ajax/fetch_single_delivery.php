<?php
include('../include/connect.php');
include('../include/function.php');
if(isset($_POST["user_id"]))
{
	$output = array();
	$statement = $db->prepare(
		"SELECT * FROM users
		WHERE id = '".$_POST["user_id"]."'
		LIMIT 1"
	);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $row)
	{
		$output["name"] = $row["name"];
		$output["phone"] = $row["phone"];
		if($row["image"] != '')
		{
			$output['user_image'] = '<img src="../upload/'.$row["image"].'" class="img-thumbnail" width="50" height="35" /><input type="hidden" name="hidden_user_image" value="'.$row["image"].'" />';
		}
		else
		{
			$output['user_image'] = '<input type="hidden" name="hidden_user_image" value="" />';
		}
	}
	echo json_encode($output);
}