<?php

include('../include/connect.php');
include("../include/function.php");

if(isset($_POST["product_id"])) {
	
	$table_name = "products";
	$image = get_image_name($table_name, $_POST["product_id"]);
	if($image != '') {
		unlink("../upload/" . $image);
	}

	$statement = $db->prepare(
		"UPDATE $table_name  SET soft_delete = :soft_delete WHERE id = :product_id"
	);
	$result = $statement->execute(
		array(
			'soft_delete' => 0,
			'product_id' => $_POST["product_id"]
		)
	);

	if(!empty($result)) {
		echo 'Data Deleted';
	}
}