<?php

include('../include/connect.php');
include("../include/function.php");

if(isset($_POST["notification_id"])) {
	
	$table_name = "message_notification";

	$statement = $db->prepare("DELETE FROM $table_name WHERE id = :notification_id");
	$result = $statement->execute(array('notification_id' => $_POST["notification_id"]));

	if(!empty($result)) {
		echo 'Data Deleted';
	}
}