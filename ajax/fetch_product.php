<?php
	include('../include/connect.php');
	include('../include/function.php');
	$query = '';
	$output = array();

	$query .= "SELECT * FROM products WHERE (status = 1 AND soft_delete = 1)";

	if(isset($_POST["search"]["value"]))
	{
		$query .= 'AND (name LIKE "%'.$_POST["search"]["value"].'%" ';
		$query .= 'OR description LIKE "%'.$_POST["search"]["value"].'%"';
		$query .= 'OR price LIKE "%'.$_POST["search"]["value"].'%")';
		
	}
	if(isset($_POST["order"]))
	{
		$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
	}
	else
	{
		$query .= 'ORDER BY id DESC ';
	}
	if($_POST["length"] != -1)
	{
		$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
	}
	$statement = $db->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$data = array();
	$filtered_rows = $statement->rowCount();


	foreach($result as $row)
	{
		$image = '';
		if($row["image"] != '')
		{
			$image = '<img src="../upload/'.$row["image"].'" class="img-thumbnail" width="50" height="35" />';
		}
		else
		{
			$image = '';
		}
		$sub_array = array();
		$sub_array[] = $row["name"];
		$sub_array[] = $row["description"];
		$sub_array[] = $row["price"];
		$sub_array[] = $image;
		
		$sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-xs update">تعديل</button>';
		$sub_array[] = '<button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger btn-xs delete">حذف</button>';
		$data[] = $sub_array;
	}

	$output = array(
		"draw"				=>	intval($_POST["draw"]),
		"recordsTotal"		=> 	$filtered_rows,
		"recordsFiltered"	=>	$filtered_rows,
		"data"				=>	$data
	);
	echo json_encode($output);