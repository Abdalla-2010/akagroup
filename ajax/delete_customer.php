<?php

include('../include/connect.php');
include("../include/function.php");

if(isset($_POST["user_id"]))
{

	$table_name = "users";

	$image = get_image_name($table_name, $_POST["user_id"]);
	if($image != '')
	{
		unlink("../upload/" . $image);
	}
	$statement = $db->prepare(
		"DELETE FROM $table_name WHERE id = :id"
	);
	$result = $statement->execute(
		array(
			':id'	=>	$_POST["user_id"]
		)
	);

	if(!empty($result))
	{
		echo 'Data Deleted';
	}
}