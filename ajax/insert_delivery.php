  
<?php
	include('../include/connect.php');
	include('../include/function.php');
	if(isset($_POST["operation"]))
	{
		if($_POST["operation"] == "Add")
		{
			$image = '';
			if($_FILES["user_image"]["name"] != '')
			{
				$image = upload_image("user_image");
			}
			$statement = $db->prepare("
				INSERT INTO users (name, phone, image,group_id,password) 
				VALUES (:name, :phone, :image,1,:password)
			");
			$result = $statement->execute(
				array(
					':name'	=>	$_POST["name"],
					':phone'	=>	$_POST["phone"],
					':image'		=>	$image,
					':password'    =>sha1($_POST["password"])
				)
			);
			if(!empty($result))
			{
				echo 'Data Inserted';
			}
		}



		if($_POST["operation"] == "Edit")
		{
			$image = '';
			if($_FILES["user_image"]["name"] != '')
			{
				$image = upload_image("user_image");
			}
			else
			{
				$image = $_POST["hidden_user_image"];
			}
			$statement = $db->prepare(
				"UPDATE users 
				SET  name = :name, phone = :phone, image = :image  
				WHERE id = :id
				"
			);
			$result = $statement->execute(
				array(
					':name'	=>	$_POST["name"],
					':phone'	=>	$_POST["phone"],
					':image'		=>	$image,
					':id'			=>	$_POST["user_id"]
				)
			);
			if(!empty($result))
			{
				echo 'Data Updated';
			}
		}
	}