<?php
include('../include/connect.php');
include('../include/function.php');
if(isset($_POST["coupon_id"])) {

	$output = array();
	$statement = $db->prepare(
		"SELECT * FROM coupon WHERE id = '".$_POST["coupon_id"]."' LIMIT 1"
	);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $row) {

		// Data
		$output["coupon_id"] = $row["id"];
		$output["name"] = $row["name"];
		$output["value"] = $row["value"];
		$output["definition"] = $row["definition"];
		$output["start_date"] = $row["start_date"];
		$output["end_date"] = $row["end_date"];
		if($row["image"] != '')
		{
			$output['coupon_image'] = '<img src="../upload/'.$row["image"].'" class="img-thumbnail" width="50" height="35" /><input type="hidden" name="hidden_coupon_image" id="hidden_coupon_image" value="'.$row["image"].'" />';
		}
		else
		{
			$output['coupon_image'] = '<input type="hidden" name="hidden_coupon_image" id="hidden_coupon_image" value="" />';
		}

	}

	echo json_encode($output);
}