  
<?php
	include('../include/connect.php');
	include('../include/function.php');
	if(isset($_POST["operation"])) {

		// Adding Data
		if($_POST["operation"] == "Add") {

			$image = '';
			if($_FILES["service_image"]["name"] != '')
			{
				$image = upload_image("service_image");
			}
			$statement = $db->prepare("
				INSERT INTO products(name, description, price, status, image) 
				VALUES (:name, :description, :price, :status, :image)
			");
			$result = $statement->execute(
				array(
					'name'	=>	$_POST["name"],
					'description'	=>	$_POST["description"],
					'price'	=>	$_POST["price"],
					'status'	=>	0,
					'image'		=>	$image
				)
			);
			if(!empty($result))
			{
				echo 'Data Inserted';
			}
		}


		// Updating Data
		if($_POST["operation"] == "Edit") {

			$image = '';
			if($_FILES["service_image"]["name"] != '')
			{
				$image = upload_image("service_image");
			}
			else
			{
				$image = $_POST["hidden_service_image"];
			}
			$statement = $db->prepare(
				"UPDATE products 
					SET  name = :name, description = :description, price = :price, image = :image  
					WHERE id = :service_id"
			);
			$result = $statement->execute(
				array(
					'name'	=>	$_POST["name"],
					'description' => $_POST["description"],
					'price' =>	$_POST["price"],
					'image' =>	$image,
					'service_id' => $_POST["service_id"]
				)
			);
			if(!empty($result))
			{
				echo 'Data Updated';
			}
		}
	}