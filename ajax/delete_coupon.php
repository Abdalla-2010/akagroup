<?php

include('../include/connect.php');
include("../include/function.php");

if(isset($_POST["coupon_id"])) {
	
	$table_name = "coupon";
	$image = get_image_name($table_name,$_POST["coupon_id"]);
	if($image != '') {
		unlink("../upload/" . $image);
	}

	$statement = $db->prepare("DELETE FROM $table_name WHERE id = :coupon_id");
	$result = $statement->execute(array('coupon_id' => $_POST["coupon_id"]));

	if(!empty($result)) {
		echo 'Data Deleted';
	}
}