<?php
include('../include/connect.php');
include('../include/function.php');
if(isset($_POST["service_id"]))
{
	$output = array();
	$statement = $db->prepare(
		"SELECT * FROM products WHERE (status = 0 AND soft_delete = 1)
			AND id = '".$_POST["service_id"]."' LIMIT 1"
	);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $row) {
		$output["name"] = $row["name"];
		$output["description"] = $row["description"];
		$output["price"] = $row["price"];
		if($row["image"] != '')
		{
			$output['service_image'] = '<img src="../upload/'.$row["image"].'" class="img-thumbnail" width="50" height="35" /><input type="hidden" name="hidden_service_image" id="hidden_service_image" value="'.$row["image"].'" />';
		}
		else
		{
			$output['service_image'] = '<input type="hidden" name="hidden_service_image" id="hidden_service_image" value="" />';
		}
	}
	echo json_encode($output);
}