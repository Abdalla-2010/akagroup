  
<?php
	include('../include/connect.php');
	include('../include/function.php');
	if(isset($_POST["operation"])) {

		// Adding Data
		if($_POST["operation"] == "Add") {

			$image = '';
			if($_FILES["coupon_image"]["name"] != '')
			{
				$image = upload_image("coupon_image");
			}
			$statement = $db->prepare("
				INSERT INTO coupon(name, value, definition, start_date, end_date, image) 
				VALUES (:name, :value, :definition, :start_date, :end_date, :image)
			");
			$result = $statement->execute(
				array(
					'name'	=>	$_POST["name"],
					'value'	=>	$_POST["value"],
					'definition'	=>	$_POST["definition"],
					'start_date'	=>	$_POST["start_date"],
					'end_date'	=>	$_POST["end_date"],
					'image'		=>	$image
				)
			);
			if(!empty($result))
			{
				echo 'Data Inserted';
			}
		}


		// Updating Data
		if($_POST["operation"] == "Edit") {

			$image = '';
			if($_FILES["coupon_image"]["name"] != '')
			{
				$image = upload_image("coupon_image");
			}
			else
			{
				$image = $_POST["hidden_coupon_image"];
			}
			$statement = $db->prepare(
				"UPDATE coupon 
					SET  name = :name, value = :value, definition = :definition, start_date = :start_date, end_date = :end_date, image = :image  
					WHERE id = :coupon_id"
			);
			$result = $statement->execute(
				array(
					'name'	=>	$_POST["name"],
					'value' => $_POST["value"],
					'definition' =>	$_POST["definition"],
					'start_date' =>	$_POST["start_date"],
					'end_date' =>	$_POST["end_date"],
					'image' =>	$image,
					'coupon_id' => $_POST["coupon_id"]
				)
			);
			if(!empty($result))
			{
				echo 'Data Updated';
			}
		}
	}