<?php
	include('../include/connect.php');
	include('../include/function.php');
	$query = '';
	$output = array();

	$query .= "SELECT orders.*, users.name AS 'user_name' FROM orders
				INNER JOIN users ON orders.user_id = users.id";
				


				
	// if(isset($_POST["search"]["value"]))
	// {
	// 	$query .= 'where orders.id LIKE "%'.$_POST["search"]["value"].'%" ';
	// }
	// if(isset($_POST["order"]))
	// {
	// 	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
	// }
	// else
	// {
	// 	$query .= 'ORDER BY orders.id DESC ';
	// }
	// if($_POST["length"] != -1)
	// {
	// 	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
	// }



	$statement = $db->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$data = array();
	$filtered_rows = $statement->rowCount();

	foreach($result as $row) {

		$result_final_product = '';
		$result_final_price_product = '';
		$TotalPrice = 0;
		$countProducts = 1;
		$quantity = 0;
		$result_final_quantity = '';
		$orderID = $row['id'];

		// Start Section Details Order
		$query_order_details = "SELECT product_id,quantity FROM order_details WHERE order_id = $orderID ";
		$statement_order_details = $db->prepare($query_order_details);
		$statement_order_details->execute();
		$result_order_details = $statement_order_details->fetchAll(PDO::FETCH_ASSOC);
		foreach($result_order_details as $row_order_details) {
			$productID = $row_order_details['product_id'];
			$quantity = $row_order_details['quantity'];

			// Getting All Quantity
			$result_final_quantity .= "(".$countProducts.") ". $quantity ."</br>";

			// Start Section Products
			$query_products = "SELECT products.name AS 'product_name',
									products.price AS 'product_price',
									products.status AS 'product_status',
									products.image AS 'product_image'
								FROM products 
									WHERE id = $productID ";
			$statement_products = $db->prepare($query_products);
			$statement_products->execute();
			$result_products = $statement_products->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($result_products as $row_products) {
				$result_final_product .= "(".$countProducts.") ".$row_products["product_name"] ."</br>";
				$result_final_price_product .= "(".$countProducts.") ".$row_products["product_price"]. " جنية" ."</br>";
				$TotalPrice += $quantity * $row_products["product_price"];
				
				$countProducts++;
			}
			// End Section Products
		}
		// End Section Details Order
		
		// Start Section Delivery
		$query_delivery = "SELECT delivery.*, users.name AS 'delivery_name', users.phone FROM delivery 
							INNER JOIN users 
								ON delivery.delivery_id = users.id
							WHERE delivery.order_id = $orderID 
								AND users.group_id = '1' LIMIT 1";
		$statement_delivery = $db->prepare($query_delivery);
		$statement_delivery->execute();
		$result_delivery = $statement_delivery->fetchAll(PDO::FETCH_ASSOC);
		$num_rows_delivery = $statement_delivery->rowCount();
		$deliveryName = '';
		if($num_rows_delivery == 1) {
			foreach($result_delivery as $row_delivery) {
				$deliveryName = $row_delivery["delivery_name"];
			}
		} else {
			$deliveryName = "لا يوجد طيار";
		}
		// End Section Delivery

		// Start Section Coupon
		$query_coupon = "SELECT coupons_order.*, coupon.name AS 'coupon_name', coupon.value As 'coupon_value' FROM coupons_order 
							INNER JOIN coupon 
								ON coupons_order.coupon_id = coupon.id
							WHERE coupons_order.order_id = $orderID LIMIT 1";
		$statement_coupon = $db->prepare($query_coupon);
		$statement_coupon->execute();
		$result_coupon = $statement_coupon->fetchAll(PDO::FETCH_ASSOC);
		$num_rows_coupon = $statement_coupon->rowCount();
		$CouponPrice = '';
		if($num_rows_coupon == 1) {
			foreach($result_coupon as $row_coupon) {
				$CouponPrice = $row_coupon["coupon_value"];
			}
		} else {
			$CouponPrice = "0";
		}
		// End Section Coupon


		// Totally Price After Discount
		$TotalPrice = $TotalPrice - $CouponPrice;

		// Set Status Of Order
		$status = $row["status_order"];
		$status_order = '';
		if($status == 0) {
			$status_order = "تم الطلب";
		} else if($status == 1) {
			$status_order = "تم القبول";
		} else if($status == 2) {
			$status_order = "جاري التوصيل";
		} else if($status == 3) {
			$status_order = "تم الاستلام";
		} else if($status == 4) {
			$status_order = "تم التوصيل";
		}

		// Data
		$sub_array = array();
		$sub_array[] = $row["id"]; // id Of Order
		$sub_array[] = $row["user_name"]; // Name Of User
		$sub_array[] = $result_final_product; // Name Of Products
		$sub_array[] = $result_final_price_product; // Price Of Products
		$sub_array[] = $result_final_quantity; // Quantity Of Order
		$sub_array[] = $CouponPrice .' جنية'; // Price Of Coupon
		$sub_array[] = $TotalPrice .' جنية'; // Totally Price Of Order
		$sub_array[] = $deliveryName; // Name Of Delivery
		$sub_array[] = $status_order; // Status Of Order
		$sub_array[] = $row["date_time"]; // DateTime Of Order
		$sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-success btn-xs view">عرض</button>
						</br></br><button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger btn-xs delete">حذف</button>';

		$data[] = $sub_array;
	}

	$output = array(
		"draw"				=>	intval($_POST["draw"]),
		"recordsTotal"		=> 	$filtered_rows,
		"recordsFiltered"	=>	get_total_all_records("orders"),
		"data"				=>	$data
	);
	echo json_encode($output);