<?php
	include('../include/connect.php');
	include('../include/function.php');
	$query = '';
	$output = array();

	$query .= "SELECT * FROM message_notification ";

	if(isset($_POST["search"]["value"]))
	{
		$query .= 'where title LIKE "%'.$_POST["search"]["value"].'%" ';
		$query .= 'OR message LIKE "%'.$_POST["search"]["value"].'%"';
	}
	if(isset($_POST["order"]))
	{
		$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
	}
	else
	{
		$query .= 'ORDER BY id DESC ';
	}
	if($_POST["length"] != -1)
	{
		$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
	}
	$statement = $db->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$data = array();
	$filtered_rows = $statement->rowCount();


	foreach($result as $row) {
		
		$sub_array = array();
		$sub_array[] = $row["title"];
		$sub_array[] = $row["message"];
		$sub_array[] = $row["date_time"];
		$sub_array[] = '<button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger btn-xs delete">حذف</button>';
		$data[] = $sub_array;
	}

	$output = array(
		"draw"				=>	intval($_POST["draw"]),
		"recordsTotal"		=> 	$filtered_rows,
		"recordsFiltered"	=>	get_total_all_records("message_notification"),
		"data"				=>	$data
	);
	echo json_encode($output);