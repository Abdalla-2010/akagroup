<?php

require_once '../firebase/./vendor/autoload.php';
include '../firebase/connection_firebase.php';
include '../firebase/helper.php';
include '../firebase/delivery.php';
include('../include/connect.php');
include("../include/function.php");

if(isset($_POST["order_id"])) {
	
	$table_name = "orders";

	$order_id = $_POST["order_id"];

	// Check Has Delivery In Order
	$query_delivery = "SELECT delivery_id FROM delivery WHERE order_id = $order_id LIMIT 1";
	$statement_delivery = $db->prepare($query_delivery);
	$statement_delivery->execute();
	$row = $statement_delivery->fetch(PDO::FETCH_ASSOC);
	$num_rows_delivery = $statement_delivery->rowCount();

	if($num_rows_delivery > 0) { // Have Delivery
		$assign_delivery_id_order = $row['delivery_id'];

		// Start Remove Order From Delivery Assign In Firebase
			$delivery = new delivery();
			$delivery->removeOrder($database, $assign_delivery_id_order, $order_id);
		// End Remove Order From Delivery Assign In Firebase
	}

	$statement = $db->prepare("DELETE FROM $table_name WHERE id = :order_id");
	$result = $statement->execute(array('order_id' => $order_id));

	if(!empty($result)) {
		echo 'Data Deleted';
	}
}