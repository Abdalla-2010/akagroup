<?php
include('../include/connect.php');
include('../include/function.php');
if(isset($_POST["product_id"])) {

	$output = array();
	$statement = $db->prepare(
		"SELECT * FROM products WHERE (status = 1 AND soft_delete = 1) AND id = '".$_POST["product_id"]."' LIMIT 1"
	);
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $row) {

		// Data
		$output["product_id"] = $row["id"];
		$output["name"] = $row["name"];
		$output["description"] = $row["description"];
		$output["price"] = $row["price"];
		if($row["image"] != '')
		{
			$output['product_image'] = '<img src="../upload/'.$row["image"].'" class="img-thumbnail" width="50" height="35" /><input type="hidden" name="hidden_product_image" id="hidden_product_image" value="'.$row["image"].'" />';
		}
		else
		{
			$output['product_image'] = '<input type="hidden" name="hidden_product_image" id="hidden_product_image" value="" />';
		}

	}

	echo json_encode($output);
}