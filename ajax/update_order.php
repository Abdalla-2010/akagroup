  
<?php
	require_once '../firebase/./vendor/autoload.php';
	include '../firebase/connection_firebase.php';
	include '../firebase/order.php';
	include '../firebase/delivery.php';
	include('../firebase/function.php');
	include('../include/connect.php');
	include('../include/function.php');

	if(isset($_POST["status_order_select"]) && isset($_POST["delivery_select"]) && isset($_POST["order_id_edit"])) {

		$order_id = $_POST["order_id_edit"];
		$delivery_id = $_POST["delivery_select"];
		$status_order = $_POST["status_order_select"];

		$token_fcm = $_POST["token_fcm"];
		$key_firebase = $_POST["key_firebase"];

		$token_delivery = '';

		// Start Updating Status Order
			$statement = $db->prepare("UPDATE orders SET status_order = :status_order 
										WHERE id = :order_id");
			$result = $statement->execute(
				array('status_order' =>	$status_order,
					'order_id' => $order_id
				)
			);

			// Start Sending Message To User & Delevery
			if($result) {
				$query_status = "SELECT name_ar FROM status_order WHERE status_order = $status_order LIMIT 1";
				$statement_status = $db->prepare($query_status);
				$statement_status->execute();
				$result_status = $statement_status->fetch(PDO::FETCH_ASSOC);
				$num_rows_status = $statement_status->rowCount();

				if($num_rows_status > 1) {
					$title_user = "App Name";
					if($status_order == 1) {
						$message_user = "تم قبول طلبك";
						sendSpicialMessage($token_fcm, $title_user, $message_user);
					} else if($status_order == 2) {
						$message_user = "جاري توصيل طلبك";
						sendSpicialMessage($token_fcm, $title_user, $message_user);
					} else if($status_order == 3) {
						$message_user = "تم استلام طلبك";
						sendSpicialMessage($token_fcm, $title_user, $message_user);
					} else if($status_order == 4) {
						$message_user = "تم توصيل طلبك بنجاح .. شكراً لك";
						sendSpicialMessage($token_fcm, $title_user, $message_user);
					}

					// Get Token Delivery
					$query_token_delivery = "SELECT token_id FROM token_users WHERE user_id = $delivery_id LIMIT 1";
					$statement_token_delivery = $db->prepare($query_token_delivery);
					$statement_token_delivery->execute();
					$result_token_delivery = $statement_token_delivery->fetch(PDO::FETCH_ASSOC);
					$num_rows_token_delivery = $statement_token_delivery->rowCount();

					if($num_rows_token_delivery > 0) { // If Have Token
						$token_delivery = $result_token_delivery['token_id'];
						$title_delivery = "App Name";

						if($status_order == 1) {
							// Send Message To Delivery
							$message_delivery = "لديك طلب جديد";
							sendSpicialMessage($token_delivery, $title_delivery, $message_delivery);
						} else if($status_order == 4) {
							// Send Message To Delivery
							$message_delivery = "تم توصيل الطلب بنجاح";
							sendSpicialMessage($token_delivery, $title_delivery, $message_delivery);
						}
						
						
					}
				}
			}
			// End Sending Message To User & Delevery

			// Start Change Status In Firebase Order
				$order = new order();
				$updateOrderData = [ 
					'status' => $status_order,
				];
				$order->update($database, $key_firebase, $updateOrderData);
			// End Change Status In Firebase Order

		// End Updating Status Order

		// Check The Order Was Delivered Or Not
		if($status_order != 4 ) { // Not Delivered

			// Check Has Delivery In Order
			$query_delivery = "SELECT delivery_id FROM delivery WHERE order_id = $order_id LIMIT 1";
			$statement_delivery = $db->prepare($query_delivery);
			$statement_delivery->execute();
			$row = $statement_delivery->fetch(PDO::FETCH_ASSOC);
			$num_rows_delivery = $statement_delivery->rowCount();

			if($num_rows_delivery > 0) { // Have Delivery

				$assign_delivery_id_order = $row['delivery_id'];

				if($delivery_id != $assign_delivery_id_order) { // If Change Assign New Delivery To Order
					// Start Remove Order From Delivery Assign In Firebase
						$delivery = new delivery();
						$delivery->removeOrder($database, $assign_delivery_id_order, $order_id);
					// End Remove Order From Delivery Assign In Firebase

					// Updating Delivery To Order
					$statement = $db->prepare("UPDATE delivery SET delivery_id = :delivery_id 
					WHERE order_id = :order_id");
					$result = $statement->execute(array('delivery_id' => $delivery_id, 'order_id' => $order_id));
				}

			} else { // No Have Delivery

				// Adding Delivery To Order
				$statement = $db->prepare("INSERT INTO delivery(order_id, delivery_id) 
				VALUES (:order_id, :delivery_id) ");
				$result = $statement->execute(
					array(
						'order_id'	=>	$order_id,
						'delivery_id' => $delivery_id
					)
				);
			}

			// Start Update Delivery To Order In Firebase
				$delivery = new delivery();
				$updateDeliveryData = [ 
					$order_id => $key_firebase,
				];
				$delivery->update($database, $delivery_id, $updateDeliveryData);
			// End Update Delivery To Order In Firebase

		} else { // Is Delivered

			$stmt_delete_delivery = $db->prepare("DELETE FROM delivery WHERE order_id = $order_id AND delivery_id = $delivery_id");
			$stmt_delete_delivery->execute();
			$count_delete_delivery = $stmt_delete_delivery->rowCount();
			 
			if($count_delete_delivery > 0) {

				// Start Remove Order From Delivery Assign In Firebase
					$delivery = new delivery();
					$delivery->removeOrder($database, $delivery_id, $order_id);
				// End Remove Order From Delivery Assign In Firebase
				
			}
		}

	}