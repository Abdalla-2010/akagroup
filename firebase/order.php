<?php

include 'helper.php';

class order { 

    function getOrder($database, $id_user) {
        $reference = $database->getReference(helper::$Refe_order.'/'.$id_user);
        $snapshot = $reference->getSnapshot();
        $value = $snapshot->getValue();
        return $value;
    }

     // Reterned Key Order
     function addOrder($database, $data) {
        $postRef = $database->getReference(helper::$Refe_order)->push($data);
        $postKey = $postRef->getKey(); // Getting Key After Puch Data
        return $postKey;
    }

    // Reterned boolean Value
    function update($database, $id_user, $data) {
        $database->getReference(helper::$Refe_order.'/'.$id_user)->update($data);
        return true;
    }
    
    // Reterned boolean Value
    function remove($database, $id_user) {
        $database->getReference(helper::$Refe_order.'/'.$id_user)->remove();
        return true;
    }

    // function hasKey($database, $id_user) {
    //     return $database->getReference(helper::$Refe_order.'/'.$id_user)->exists();
    // }
    
}