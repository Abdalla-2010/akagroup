<?php

include 'helper.php';

class fcm {
    function pushNotificationToAll() {
        $title = 'My Notification Title';
        $body = 'My Notification Body';
        $imageUrl = 'http://lorempixel.com/400/200/';
        
        $notification = Notification::fromArray([
            'title' => $title,
            'body' => $body,
            'image' => $imageUrl,
        ]);
        
        $notification = Notification::create($title, $body);

        $message = $message->withNotification($notification);
    }

    function pushNotificationToSpecificDevices($deviceToken) {
        $message = CloudMessage::withTarget('token', $deviceToken)
        ->withNotification($notification) // optional
        ->withData($data) // optional
        ;

        $messaging->send($message);
    }
}