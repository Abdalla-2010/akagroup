<?php

    function sendSpicialMessage($token_id, $title, $message) {

        $token_credentials = "AAAAa-pGgno:APA91bF59XVtu4NVmfst863VE3ezNUKm9dwBMLcNYXS1hev5ifZoAF4Hlx1R8gtnLRxW1FwiWDAmivMoYbH0-UGIs5SQ7oLfC0RjbLbdnzf6tblPE_lEBRn1z8ZmHC3d7C-9k4DR7flR";

        $reg_id = array();
        $reg_id[0] = $token_id;
        
        $fields = array(
            'registration_ids' => $reg_id,
            'notification' => array (
                    'body' => $message,
                    'title' => $title,
                    'icon' => 'myicon')
        );
    
        $url             = 'https://fcm.googleapis.com/fcm/send';
        $headers         = array(
            'Authorization: key='. $token_credentials ,
            'Content-Type: application/json'
        );
    
        // CURL To Open a Connection
        $ch = curl_init();
    
        // Setting the Curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
    
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
            curl_close($ch);
            return false;
        } else {
            return true;
        }

    }


    function sendMessage($db, $title, $message) {

        $token_credentials = "AAAAa-pGgno:APA91bF59XVtu4NVmfst863VE3ezNUKm9dwBMLcNYXS1hev5ifZoAF4Hlx1R8gtnLRxW1FwiWDAmivMoYbH0-UGIs5SQ7oLfC0RjbLbdnzf6tblPE_lEBRn1z8ZmHC3d7C-9k4DR7flR";

        // Getting All Token Of Users
        $query_tokens = "SELECT token_id FROM token_users";
		$statement_tokens = $db->prepare($query_tokens);
		$statement_tokens->execute();
		$result = $statement_tokens->fetchAll(PDO::FETCH_ASSOC);
        $num_rows_tokens = $statement_tokens->rowCount();
        $i      = 0;
        $reg_id = array();

        if ($num_rows_tokens > 0) {
            foreach($result as $row) {
                $reg_id[$i] = $row['token_id'];
                $i++;
            }
        }
    
        $fields = array(
            'registration_ids' => $reg_id,
            'notification' => array (
                    'body' => $message,
                    'title' => $title,
                    'icon' => 'myicon')
        );
    
        $url             = 'https://fcm.googleapis.com/fcm/send';
        $headers         = array(
            'Authorization: key='. $token_credentials ,
            'Content-Type: application/json'
        );
    
        // CURL To Open a Connection
        $ch = curl_init();
    
        // Setting the Curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
    
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    
        $response = json_decode($result, true);
    
        
        // Adding Message To Message Notification
        $statement = $db->prepare("INSERT INTO message_notification(message, title) 
        VALUES (:message, :title) ");
        $result = $statement->execute(
            array(
                'message'	=>	$message,
                'title' => $title
            )
        );
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

?>