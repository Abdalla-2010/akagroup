<?php

// include 'helper.php';

class delivery { 

    function getDelivery($database, $delivery_id) {
        $reference = $database->getReference(helper::$Refe_delivery.'/'.$delivery_id);
        $snapshot = $reference->getSnapshot();
        $value = $snapshot->getValue();
        return $value;
    }

     // Reterned Key Delivery
     function addDelivery($database, $delivery_id, $data) {
        $postRef = $database->getReference(helper::$Refe_delivery.'/'.$delivery_id.'/'.$data);
        return true;
    }

    // Reterned boolean Value
    function update($database, $delivery_id, $data) {
        $database->getReference(helper::$Refe_delivery.'/'.$delivery_id)->update($data);
        return true;
    }
    
    // Reterned boolean Value
    function remove($database, $delivery_id) {
        $database->getReference(helper::$Refe_delivery.'/'.$delivery_id)->remove();
        return true;
    }

    // Reterned boolean Value
    function removeOrder($database, $delivery_id, $key) {
        $database->getReference(helper::$Refe_delivery.'/'.$delivery_id.'/'.$key)->remove();
        return true;
    }

    // function hasKey($database, $id_user) {
    //     return $database->getReference(helper::$Refe_delivery.'/'.$id_user)->exists();
    // }
    
}